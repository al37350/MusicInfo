from .app import manager, db

@manager.command
def loaddb(filename):
	'''creates the tables and populates them with db'''

	db.create_all()

	import yaml
	albums=yaml.load(open(filename, encoding="utf-8"))

	from .models import Genre, Album, Artiste

	
	genresConnus={}
	artistes={}

	for m in albums:

		#ajout des artistes
		a=m["by"]
		if a not in artistes:
			o=Artiste(name=a)
			db.session.add(o)
			artistes[a]=o


		#ajout des genres
		genres=m["genre"]
		for genre in genres:	
			if genre.lower() not in genresConnus:
				o=Genre(name=genre.lower())
				db.session.add(o)
				genresConnus[genre.lower()]=o
	db.session.commit()
	# print(genresConnus)
	
	for m in albums:
		test=[]
		for genre in m["genre"]:
			test.append(genresConnus[genre.lower()])
		#ajout des albums
		a = artistes[m["by"]]
		# print(a.id)
		o= Album(img=m["img"],
			title=m["title"],
			year=m["releaseYear"],
			by=m["by"],
			artiste_id=a.id,
			children=test,
			noteMoy=None)
		db.session.add(o)
	db.session.commit()

@manager.command
def syncdb():
	db.create_all()


@manager.command
def newuser(username, password):
	from .models import User
	from hashlib import sha256
	m=sha256()
	m.update(password.encode())
	liste=[]
	u=User(username=username, password=m.hexdigest(), favoris=liste)
	db.session.add(u)
	db.session.commit()