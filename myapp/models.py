from .app import db, login_manager
from sqlalchemy import *
import json
from urllib.request import urlopen,quote

association_table = db.Table('association', db.Model.metadata,
    Column('genre_id', Integer, ForeignKey('genre.id')),
    Column('album_id', Integer, ForeignKey('album.id'))
)

association_tableFavoris = db.Table('associationFavoris', db.Model.metadata,
    Column('album_id', Integer, ForeignKey('album.id')),
    Column('user_id', Integer, ForeignKey('user.username'))
)

class Genre(db.Model):
	__tablename__ = 'genre'
	def __repr__(self):
		return "<Genre (%d) %s" %(self.id, self.name)

	id=db.Column(db.Integer, primary_key=True)
	name=db.Column(db.String(100))


class Artiste(db.Model):
	def __repr__(self):
		return "<Artiste (%d) %s" %(self.id, self.name)

	id=db.Column(db.Integer, primary_key=True)
	name=db.Column(db.String(100))


class Album(db.Model):
	__tablename__ = 'album'
	def __repr__(self):
		return "<Album (%d) %s" %(self.id, self.title)

	id=db.Column(db.Integer, primary_key=True)
	img=db.Column(db.String(100))
	title=db.Column(db.String(100))
	year=db.Column(db.Integer)
	by=db.Column(db.String(100))
	artiste_id= db.Column(db.Integer, db.ForeignKey("artiste.id"))
	noteMoy= db.Column(db.Integer)
	artiste= db.relationship("Artiste",
		backref=db.backref("albums", lazy="dynamic"))
	children = db.relationship("Genre",
	                    secondary=association_table,
	                    backref="parents")
	
	def add_commentaire(self, commentaire):
		self.commentaires.append(commentaire)
		db.session.commit()

	def add_note(self, note):
		if self.noteMoy==None:
			self.noteMoy=note/get_nbNoteAlbum(self.id)
		else:
			self.noteMoy=(self.noteMoy*(get_nbNoteAlbum(self.id)-1)+note)/get_nbNoteAlbum(self.id)
		db.session.commit()


class Commentaire(db.Model):
	__tablename__ = 'commentaire'
	def __repr__(self):
		return "<Commentaire (%d) %s" %(self.id, self.titre)

	id=db.Column(db.Integer, primary_key=True)
	titre=db.Column(db.String(100))
	texte=db.Column(db.String(5000))
	album_id = Column(Integer, ForeignKey('album.id'))
	album = db.relationship("Album", backref="commentaires")
	user_id = db.Column(Integer, ForeignKey('user.username'))
	# commentairesEnfants_id = db.Column(Integer,ForeignKey("Commentaire.id"))
	# commentairesEnfants = db.relationship("Commentaire", backref="parent",remote_side="Commentaire.id")
	commentaireParent_id = db.Column(Integer, ForeignKey('commentaire.id'))
	children = db.relationship("Commentaire",backref=db.backref('parent', remote_side=[id]))

def createCommentaire2(titre, texte, id, userid, idParent):
	u=Commentaire(titre=titre, texte=texte, album_id=id, user_id=userid, commentaireParent_id=idParent)
	db.session.add(u)
	db.session.commit()

def createCommentaire1(titre, texte, id, userid):
	u=Commentaire(titre=titre, texte=texte, album_id=id, user_id=userid)
	db.session.add(u)
	db.session.commit()

def getCommentaire():
	return Commentaire.query.all()

class Note(db.Model):
	__tablename__ = 'note'
	def __repr__(self):
		return "Note : %d User : %s Album : %d" %(self.valeur, self.user_id, self.album_id)
	album_id= Column(Integer, ForeignKey('album.id'),primary_key=True)
	user_id = db.Column(Integer, ForeignKey('user.username'),primary_key=True)
	valeur = db.Column(db.Integer)

def createNote(username, albumid, valeur):
	n=Note(album_id=albumid, user_id=username, valeur=valeur)
	db.session.add(n)
	db.session.commit()
	return n

def is_notexistNote(username, albumid):
	return Note.query.filter(Note.user_id==username, Note.album_id==albumid).count()==0

def get_nbNoteAlbum(albumid):
	return Note.query.filter(Note.album_id==albumid).count()

def get_noteUserAlbum(username, albumid):
	if not is_notexistNote(username, albumid):
		return db.session.query(Note.valeur).filter(Note.user_id==username, Note.album_id==albumid).first()[0]
	else:
		return None

from flask.ext.login import UserMixin
class User(db.Model, UserMixin):
	__tablename__ = 'user'
	username=db.Column(db.String(50), primary_key=True)
	password= db.Column(db.String(64))
	favoris = db.relationship("Album",
	                    secondary=association_tableFavoris,
	                    backref="aimerPar")
	notes=db.relationship("Note")
	def get_id(self):
		return self.username

	def get_favoris(self, page, nbParPage):
		return Pagination(self.favoris, page, nbParPage)

	def add_favoris(self, album):
		self.favoris.append(album)
		db.session.commit()

	def have_favoris(self, id):
		return get_album(id) in self.favoris

	def delete_favoris(self, album):
		self.favoris.remove(album)
		db.session.commit()

	def add_note(self, note):
		self.notes.append(note)
		db.session.commit()

@login_manager.user_loader
def load_user(username):
	return User.query.get(username)
	
def get_albumAlea(nb):
	return Album.query.order_by(func.random()).limit(nb).all()

def get_albumName(name, page, nbParPage):
	return Album.query.filter(Album.title.like("%"+name+"%")).paginate(page, nbParPage)

def get_albumByGenre(critere,page, nbParPage):
	#a optimiser
	genreCorrespondant=Genre.query.filter(Genre.name.like("%"+critere+"%")).all()
	res=[]
	ensemble=set()
	for genre in genreCorrespondant:
		for musiqueCorrespondante in get_AlbumByIdGenre(genre.id, page, nbParPage).items:
			if musiqueCorrespondante not in ensemble:
				res.append(musiqueCorrespondante)
				ensemble.add(musiqueCorrespondante)
	return Pagination(res, page, nbParPage)

def get_albumByArtiste(critere, page, nbParPage):
	artisteCorrespondant=Artiste.query.filter(Artiste.name.like("%"+critere+"%")).all()
	res=[]
	for artiste in artisteCorrespondant:
		for musiqueCorrespondante in get_AlbumByIdArtiste(artiste.id):
			res.append(musiqueCorrespondante)
	return Pagination(res, page, nbParPage)

def get_musiqueAlea(nb):
	return Musique.query.order_by(func.random()).limit(nb).all()

def get_album(id):
	return Album.query.get(id)

def get_genre(id):
	return Genre.query.get(id)

def get_artiste(id):
	return Artiste.query.get(id)

def get_allgenre(page, nbParPage):
	return Genre.query.paginate(page, nbParPage)

def get_AlbumByIdGenre(numero,page, nbParPage):
	return Album.query.join(Genre.parents).filter(Genre.id == numero).paginate(page, nbParPage)

def get_commentaires(idAlbum, page, nbParPage):
	return Album.query.get(idAlbum).paginate(page, nbParPage)

def get_AlbumByIdArtiste(numero):
	return Album.query.join(Artiste.albums).filter(Artiste.id==numero).all()

def get_idalbum_deezer(album):
	titre=quote(album.title)
	artiste=quote(album.by)
	url=("https://api.deezer.com/search?q=artist:'"+artiste+"'%20album:'"+titre+"'")
	response = urlopen(url).read().decode('utf8')
	obj = json.loads(response)
	if len(obj['data'])!=0:
		iddeezer=obj['data'][0]['album']['id']
	else:
		iddeezer=None
	return iddeezer

def get_infoArtiste_deezer(nom):
	artiste=quote(nom)
	url=("https://api.deezer.com/search/artist?q='"+artiste+"'")
	print(artiste)
	print(url)
	response = urlopen(url).read().decode('utf8')
	obj = json.loads(response)
	if len(obj['data'])!=0:
		info=obj['data'][0]
	else:
		info=None
	return info

from wtforms import PasswordField, StringField, HiddenField, RadioField, IntegerField
from hashlib import sha256
from flask.ext.wtf import Form

class LoginForm(Form):
	username=StringField('Username')
	password=PasswordField('Password')

	def get_authenticated_user(self):
		user=User.query.get(self.username.data)
		if user is None:
			return None
		m=sha256()
		m.update(self.password.data.encode())
		passwd=m.hexdigest()
		return user if passwd==user.password else None

from wtforms.validators import *
class SignInForm(Form):
	username=StringField('Username', validators=[DataRequired()])
	password=StringField('Password', validators=[DataRequired()])


	def userExiste(self):
		return User.query.get(self.username.data)

	def get_authenticated_user(self):
		user=User.query.get(self.username.data)
		if user is None:
			return None
		m=sha256()
		m.update(self.password.data.encode())
		passwd=m.hexdigest()
		return user if passwd==user.password else None

class RechercheForm(Form):
	recherche=StringField('Recherche', validators=[InputRequired()])
	options = RadioField(
        'Options de recherches',
        choices=[('parAlbum', 'Album'), ('parGenre', 'Genre'), ('parArtiste', 'Artiste')], default='parAlbum'
    )

from werkzeug.datastructures import MultiDict
class CommentaireForm(Form):
	titre=StringField('Titre', validators=[DataRequired()])
	texte=StringField('Texte', validators=[DataRequired()])
	commentaireParent=StringField('idParent')

	def reset(self):
		blankData = MultiDict([  ])
		self.process(blankData)


from math import ceil
class Pagination(object):

    def __init__(self, liste, page, per_page):
        self.liste=liste
        self.page = page
        self.per_page = per_page
        self.total_count = len(liste)

    @property
    def pages(self):
        return int(ceil(self.total_count / float(self.per_page)))

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.pages

    @property
    def items(self):
    	return self.liste[(self.page-1)*self.per_page:(self.page)*self.per_page]

    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=5, right_edge=2):
        last = 0
        for num in range(1, self.pages + 1):
            if num <= left_edge or \
               (num > self.page - left_current - 1 and \
                num < self.page + right_current) or \
               num > self.pages - right_edge:
                if last + 1 != num:
                    yield None
                yield num
                last = num