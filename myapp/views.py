from .app import app
from flask import url_for, redirect, render_template, request, flash
from .models import *
import json
from urllib.request import urlopen
@app.route("/")
def home():
	f=RechercheForm()
	return render_template("home.html", title="MusicInfo",form=f, alea=get_albumAlea(1)[0], musale=get_albumAlea(6))

@app.route("/mentions-legales/")
def mentions():
	f=RechercheForm()
	return render_template("mentions.html", title="MusicInfo", form=f)

@app.route("/album/<int:id>/")
def album(id):
	if(current_user.is_authenticated):
		note=get_noteUserAlbum(current_user.get_id(), id)
	else:
		note=None
	album=get_album(id)
	f=RechercheForm()
	commentaireFormulaire=CommentaireForm()
	return render_template("album.html", title=album.title, form=f, commentaireFormulaire=commentaireFormulaire, album=album, playerid=get_idalbum_deezer(album), note=note)

@app.route("/album/<int:id>/favoris/<favoris>")
def albumEtFavoris(id, favoris):
	album=get_album(id)
	if(favoris != None):
		if(favoris=="true"):
			current_user.add_favoris(album)
			flash(u'L\'album a bien été rajouté aux favoris')
		else:
			current_user.delete_favoris(album)
			flash(u'L\'album a bien été supprimé des favoris')

	f=RechercheForm()
	commentaireFormulaire=CommentaireForm()
	return redirect(url_for("album", id=id))

@app.route("/album/<int:id>/note/<int:note>")
def albumEtNote(id,note):
	album=get_album(id)
	
	if(note != None):
		if(is_notexistNote(current_user.get_id(), id)):
			n=createNote(current_user.get_id(), id, note)
			current_user.add_note(n)
			album.add_note(note)
			
	f=RechercheForm()
	commentaireFormulaire=CommentaireForm()
	return redirect(url_for("album", id=id))		

 
@app.route("/album/<int:id>/", methods=("POST",))
def comment(id):
	f=RechercheForm()
	commentaireFormulaire=CommentaireForm()
	album=get_album(id)
	if commentaireFormulaire.validate_on_submit():
		if(commentaireFormulaire.commentaireParent.data==""):
			createCommentaire1(commentaireFormulaire.titre.data, commentaireFormulaire.texte.data, id, current_user.get_id())
		else:
			createCommentaire2(commentaireFormulaire.titre.data, commentaireFormulaire.texte.data, id, current_user.get_id(),int(commentaireFormulaire.commentaireParent.data))
		commentaireFormulaire.reset()
	return render_template("album.html", title=album.title, form=f, commentaireFormulaire=commentaireFormulaire, album=album, playerid=get_idalbum_deezer(album))

from flask.ext.paginate import Pagination
@app.route("/genre/<int:id>")
def unGenre(id):
	f=RechercheForm()
	search = False
	try:
		page = int(request.args.get('page', 1))
	except ValueError:
		page = 1
	albums=get_AlbumByIdGenre(id, page, 20)
	genre=get_genre(id)
	# pagination = Pagination(page=page, total=len(albums), per_page=50, search=search, record_name='users')
	return render_template("unGenre.html", id=id, nomPage="genre/", title="Genre", form=f, pagination=albums, genre=genre)

@app.route("/genre/")
def genre():
	try:
		page = int(request.args.get('page', 1))
	except ValueError:
		page = 1
	genre=get_allgenre(page,20)
	f=RechercheForm()
	return render_template("genre.html", nomPage="genre", title="genres", form=f, pagination=genre)

from flask.ext.login import login_user, logout_user, current_user

@app.route("/recherche/", methods=("GET", "POST",))
def recherche():
	f=RechercheForm()
	datas=None
	if(f.recherche.data!=None and len(f.recherche.data)!=0):
		res=f.recherche.data
	elif(request.args.get('recherche')):
		res=request.args.get('recherche')

	nbParPage=20
	try:
		page = int(request.args.get('page', 1))
	except ValueError:
		page = 1
	if(f.options.data=='parGenre'):
		datas=get_albumByGenre(res,page,nbParPage)
	elif(f.options.data=='parAlbum'):
		datas=get_albumName(res,page,nbParPage)
	else:
		datas=get_albumByArtiste(res,page,nbParPage)
	return render_template("recherche.html", params="&recherche="+res, nomPage="recherche", title="Rechercher un album", form=f, pagination=datas)

@app.route("/login/", methods=("GET", "POST",))
@app.route("/login/<path:code>", methods=("GET", "POST",))
def login(code=None):
    f2=LoginForm()
    f=RechercheForm()
    if f2.validate_on_submit():
        user=f2.get_authenticated_user()
        if user:
            login_user(user)
            if(code==None):
                return redirect(url_for("home"))
            else:
                return redirect(code)
        flash(u'Votre login ou votre mot de passe est incorrect.')
    return render_template("login.html", path=code, form=f, form2=f2)

from .commands import *
@app.route("/signin/", methods=("GET", "POST",))
def signin():
	f2=SignInForm()
	f=RechercheForm()
	if f2.validate_on_submit():
		user=f2.userExiste()
		if user==None:
		 	newuser(f2.username.data, f2.password.data)
		userCreer=f2.get_authenticated_user()
		login_user(userCreer)
		return redirect(url_for("home"))
	return render_template("signin.html", form=f, form2=f2)


@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for("home"))

@app.route("/favoris/")
def favoris():
	try:
		page = int(request.args.get('page', 1))
	except ValueError:
		page = 1
	f=RechercheForm()
	return render_template("favoris.html", form=f,nomPage="favoris", pagination=current_user.get_favoris(page,20))

@app.route("/artiste/<int:id>")
def artiste(id):
	artiste=get_artiste(id)
	albums=get_AlbumByIdArtiste(id)
	f=RechercheForm()
	return render_template("artiste.html", form=f, title=artiste.name, artiste=artiste, albums=albums,info=get_infoArtiste_deezer(artiste.name))